//
//  AppDelegate.h
//  Clock
//
//  Created by Stan on 2017-02-03.
//  Copyright © 2017 stan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

